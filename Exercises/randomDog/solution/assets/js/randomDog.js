var btn = document.querySelector("#btn");
var image = document.querySelector("#photo");
btn.addEventListener("click", function() {
  var XHR = new XMLHttpRequest();

  XHR.onreadystatechange = function () {
    if(XHR.readyState == 4){
      if(XHR.status == 200){
        image.src = JSON.parse(XHR.responseText).message;
      }
    }
  }
  XHR.open("GET", "https://dog.ceo/api/breeds/image/random");
  XHR.send();
});
