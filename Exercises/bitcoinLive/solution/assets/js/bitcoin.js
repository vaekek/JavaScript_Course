var button = document.querySelector("#reload-coin");
var label = document.querySelector("#price");

button.addEventListener("click", function () {
  var XHR = new XMLHttpRequest();
  XHR.onreadystatechange = function(){
    if(XHR.readyState == 4){
      if(XHR.status == 200){
        label.innerHTML = JSON.parse(XHR.responseText).bpi.USD.rate + " USD";
      }
    }
  }
  XHR.open("GET", "https://api.coindesk.com/v1/bpi/currentprice.json");
  XHR.send();
});
