/* Here you have to do an XMLHTTPRequest and check if the readyState is 4, and
the status is 200. So the Request is done. If The readyState is 4 and the status
is 200 you console.log the responseText from the Request.
If its not 4 and 200 you return an Error Message.

-- The API from github will response a random quote.
*/

var XHR =  new XMLHTTPRequest();

XHR.onreadyStateChange = function () {

}

/*
*params{
Method,
URL
}
*/
XHR.open("GET", "https://api.github.com/zen");
XHR.send();
