/* Here you gonna build a randomUser Generator with a fetch call to an API.
We're going to build a widget where you can click on the button (The HTML & CSS is already made)
and recieve the data from an API and instantly update the widget after getting the data.
*/

const url = "https://randomuser.me/api/";
const btn = document.querySelector("#btn");
const name = document.querySelector("#fullname");
const username = document.querySelector("#username");
const email = document.querySelector("#email");
const city = document.querySelector("#city");
const avatar = document.querySelector("#avatar");

btn.addEventListener("click", () => {
  fetch(url)
    .then(handleErrors)
    .then(parseJSON)
    .then(updateProfile)
    .catch(displayErrors)
});

handleErrors = (res) => {
  if(!res.ok){
    throw Error(res.status);
  }
  return res;
}

parseJSON = (res) => {
  return res.json().then((parsedData) => {
    return parsedData.results[0];
  });
}

updateProfile = (data) => {
  name.innerText = data.name.first + " " + data.name.last;
  avatar.src = data.picture.medium;
  username.innerText = data.login.username;
  city.innerText = data.location.city;
  email.innerText = data.email;
}

displayErrors = (err) => {
  console.log(err);
}
